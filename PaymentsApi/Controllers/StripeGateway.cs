﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Stripe;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("stripe")]
    public class StripeGatewayController : Controller
    {
        [HttpGet("firstPayment")]
        public ActionResult Get()
        {
            var customer = SetCustomer();
            var intent = PaymentIntent(customer.Id);
            return Json(new { client_secret = intent.ClientSecret, customer_id = customer.Id });
        }

        [HttpGet("paymentIban")]
        public ActionResult GetIntentIban(string customerId, string source)
        {
            var customer = SetCustomer();
            var charge = PaymentIntentIban(customerId, source);
            return Json(new { status = charge.Status, customer_id = customer.Id });
        }

        [HttpGet("customer")]
        public ActionResult Customer()
        {
            //var customer = "cus_HMlqhRcc7NE5Z9";
            var customer = SetCustomer();
            return Json(new { client_secret = customer.Id });
        }

        [HttpGet("paymentCustomer")]
        public ActionResult paymentCustomer()
        {
            var intent = PaymentIntentCustomer();
            return Json(new { client_secret = intent.ClientSecret });
        }

        [HttpGet("getPaymentCardForCustomer")]
        public ActionResult getPaymentCardForCustomer(String customerId)
        {
            var c = getCardsForCustomer(customerId);
            return Json(c);
        }

        public StripeList<PaymentMethod> getCardsForCustomer(String customerId)
        {
            StripeConfiguration.ApiKey = "sk_test_IMpCa657HXN4e2XicGpnd89c00II5glIcX";
            var options = new PaymentMethodListOptions
            {
                Customer = customerId,
                Type = "card",
            };

            var service = new PaymentMethodService();
            var paymentMethods = service.List(options);

            return paymentMethods;

        }

        [HttpGet("payment")]
        public ActionResult payment()
        {
            SetPaymentMethod();
            return Json(new { client_secret = "true" });
        }

        public PaymentIntent PaymentIntent(String customerId)
        {
     //       StripeConfiguration.ApiKey = "sk_test_L6nlkxwdHGd6ABGhsRllhMDA";
            StripeConfiguration.ApiKey = "sk_test_IMpCa657HXN4e2XicGpnd89c00II5glIcX";
            var customerOptions = new CustomerCreateOptions { };

            var options = new PaymentIntentCreateOptions
            {
                Amount = 100,
                Currency = "eur", 
                Customer = customerId,
                PaymentMethodTypes = new List<string> {
                    "card",
                  },
            };

            var service = new PaymentIntentService();
            var intent = service.Create(options);
            return intent;
        }

        public Charge PaymentIntentIban(String customerId, string source)
        {
            StripeConfiguration.ApiKey = "sk_test_L6nlkxwdHGd6ABGhsRllhMDA";
            var customerOptions = new CustomerCreateOptions { };

            /*   var options = new PaymentIntentCreateOptions
               {
                   Amount = 100,
                   Currency = "eur",
                   Customer = customerId,
                   PaymentMethodTypes = new List<string> {
                       "sepa_debit",
                     },
               }; 
            */
            var options = new ChargeCreateOptions
            {
                Amount = 1099,
                Currency = "eur",
                Customer = customerId,
                Source = source,
            };

            var service = new ChargeService();
            Charge charge = service.Create(options);
           
            return charge;
        }


        public PaymentIntent PaymentIntentCustomer()
        {
            StripeConfiguration.ApiKey = "sk_test_IMpCa657HXN4e2XicGpnd89c00II5glIcX";

            var options = new PaymentIntentCreateOptions
            {
                Amount = 1099,
                Currency = "eur",
                Customer = "cus_Hbo7UwF2VMTICf",
                PaymentMethod = "src_1H2aUxHO8ZP0RqsJpWSAA5s6",
                OffSession = true,
                Confirm = true
           
            };

            var service = new PaymentIntentService();
            var intent = service.Create(options);
            return intent;
        }
        public Customer SetCustomer()
        {
            //StripeConfiguration.ApiKey = "sk_test_L6nlkxwdHGd6ABGhsRllhMDA";
            StripeConfiguration.ApiKey = "sk_test_IMpCa657HXN4e2XicGpnd89c00II5glIcX";
            var customerOptions = new CustomerCreateOptions {Name ="AntonioCustomer"  , Email = "antonio.esposito@osteocom.me"};
            var customerService = new CustomerService();
            var customer = customerService.Create(customerOptions);
           
            return customer;
        }

        public Customer SetIbanCustomer()
        {
            //StripeConfiguration.ApiKey = "sk_test_L6nlkxwdHGd6ABGhsRllhMDA";
            StripeConfiguration.ApiKey = "sk_test_L6nlkxwdHGd6ABGhsRllhMDA";
            var customerOptions = new CustomerCreateOptions { Name = "AntonioCustomer", Email = "antonio.esposito@osteocom.me" };
            var customerService = new CustomerService();
            var customer = customerService.Create(customerOptions);

            return customer;
        }

        public Customer SetPaymentMethodCustomer()
        {
            StripeConfiguration.ApiKey = "sk_test_L6nlkxwdHGd6ABGhsRllhMDA";
            var customerOptions = new CustomerCreateOptions { Name = "AntonioCustomer", Email = "antonio.esposito@osteocom.me" };
            var customerService = new CustomerService();
            var customer = customerService.Create(customerOptions);

            return customer;
        }
        public void SetPaymentMethod()
        {
            StripeConfiguration.ApiKey = "sk_test_L6nlkxwdHGd6ABGhsRllhMDA";
            var options = new PaymentMethodListOptions
            {
                Customer = "cus_HMmPmk01RAbFmJ",
                Type = "card",
            };

            var service = new PaymentMethodService();
            var paymentMethods = service.List(options);
            var a = paymentMethods;           
        }
    }
}
