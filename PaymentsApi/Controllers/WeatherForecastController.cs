﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Stripe;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("secret")]
    public class WeatherForecastController : Controller
    {
        [HttpGet("firstPayment")]
        public ActionResult Get()
        {
            var intent = PaymentIntent();
            return Json(new { client_secret = intent.ClientSecret });
        }

        [HttpGet("customer")]
        public ActionResult Customer()
        {
            //var customer = "cus_HMlqhRcc7NE5Z9";
            var customer = SetCustomer();
            return Json(new { client_secret = customer.Id });
        }

        [HttpGet("paymentCustomer")]
        public ActionResult paymentCustomer()
        {
            var intent = PaymentIntentCustomer();
            return Json(new { client_secret = intent.ClientSecret });
        }

        [HttpGet("payment")]
        public ActionResult payment()
        {
            SetPaymentMethod();
            return Json(new { client_secret = "true" });
        }


        public PaymentIntent PaymentIntent()
        {
            StripeConfiguration.ApiKey = "sk_test_L6nlkxwdHGd6ABGhsRllhMDA";
            var customerOptions = new CustomerCreateOptions { };

            var customer = SetCustomer();

            var options = new PaymentIntentCreateOptions
            {
                Amount = 100,
                Currency = "eur", 
                Customer = customer.Id,
                PaymentMethodTypes = new List<string> {
                    "card",
                  },
            };

            var service = new PaymentIntentService();
            var intent = service.Create(options);
            return intent;
        }

        public PaymentIntent PaymentIntentCustomer()
        {
            StripeConfiguration.ApiKey = "sk_test_L6nlkxwdHGd6ABGhsRllhMDA";

            var options = new PaymentIntentCreateOptions
            {
                Amount = 1099,
                Currency = "eur",
                Customer = "cus_HMpTR1PpPxasDA",
                PaymentMethod = "pm_1Go5poHO8ZP0RqsJKP1dzXpP",
                OffSession = true,
                Confirm = true
           
            };

            var service = new PaymentIntentService();
            var intent = service.Create(options);
            return intent;
        }
        public Customer SetCustomer()
        {
            StripeConfiguration.ApiKey = "sk_test_L6nlkxwdHGd6ABGhsRllhMDA";
            var customerOptions = new CustomerCreateOptions {Name ="AntonioCustomer"  , Email = "antonio.esposito@osteocom.me"};
            var customerService = new CustomerService();
            var customer = customerService.Create(customerOptions);
           
            return customer;
        }

        public Customer SetPaymentMethodCustomer()
        {
            StripeConfiguration.ApiKey = "sk_test_L6nlkxwdHGd6ABGhsRllhMDA";
            var customerOptions = new CustomerCreateOptions { Name = "AntonioCustomer", Email = "antonio.esposito@osteocom.me" };
            var customerService = new CustomerService();
            var customer = customerService.Create(customerOptions);

            return customer;
        }
        public void SetPaymentMethod()
        {
            StripeConfiguration.ApiKey = "sk_test_L6nlkxwdHGd6ABGhsRllhMDA";
            var options = new PaymentMethodListOptions
            {
                Customer = "cus_HMmPmk01RAbFmJ",
                Type = "card",
            };

            var service = new PaymentMethodService();
            var paymentMethods = service.List(options);
            var a = paymentMethods;           
        }
    }
}
